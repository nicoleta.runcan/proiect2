import java.awt.event.ActionEvent;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Button3 implements ActionListener{
	JPanel panel1;
	JPanel panel2;
	public Button3(JPanel panel1,JPanel panel2) {
		this.panel1=panel1;
		this.panel2=panel2;
	}
	public void actionPerformed(ActionEvent e) {
		Random random=new Random();
		panel1.setBackground(new Color(random.nextFloat(),random.nextFloat(),random.nextFloat()));
		panel2.setBackground(new Color(random.nextFloat(),random.nextFloat(),random.nextFloat()));
	}

}
