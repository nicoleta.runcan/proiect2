import java.awt.event.ActionEvent;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Button2 implements ActionListener {
	
	private JLabel label;
	private JTextField tf;
	public Button2(JLabel label,JTextField tf) {
		this.label=label;
		this.tf=tf;
	}
	public void actionPerformed(ActionEvent e) {
		String s=tf.getText();
		label.setText(s);
	}

}
